class Course:  # Product
    def __init__(self, id, coursename, coursedesc):
        self.__id = id
        self.__courseName = coursename
        self.__courseDesc = coursedesc
        self.__enrollments = []  # An empty list of all the enrollments

    @property
    def id(self):
        return self.__id

    @property
    def courseName(self):
        return self.__courseName

    @property
    def courseDesc(self):
        return self.__courseDesc
