from Course import Course
from ClassEnrollment import ClassEnrollment
from Student import Student


def printOutStudentInformation(data):
    student = data[0]
    print("\n" + student.firstname + " " + student.lastname + " ("+str(student.cwid) + ")")
    for i in range(1, len(data)):
        classEnrollment = data[i]
        course = classEnrollment.course
        print("    Class Enrollment: " + str(classEnrollment.id) + " with a grade of " + classEnrollment.grade)
        print("        Course Info: " + course.id
              + " " + course.courseName + ": " + course.courseDesc)


# Student definitions
student1 = Student(890457906, "Alexander", "Sigler")
student2 = Student(890457905, "Bob", "Sigler")
student3 = Student(890457909, "John", "Sigler")

# Course Definitions
course1 = Course("CPSC223P", "Python", "Basics to Python")
course2 = Course("CPSC351", "Operating Systems", "Introduction to multithreading and multiprocessing")
course3 = Course("EGCP450", "Embdedded Systems", "Programming using microcontrollers")

# Student 1 Enrollment
s1classEnrollment1 = ClassEnrollment(17540, "A-")
s1classEnrollment1.setCourse(course1)

s1classEnrollment2 = ClassEnrollment(18460, "C")
s1classEnrollment2.setCourse(course2)

s1classEnrollment3 = ClassEnrollment(98725, "B+")
s1classEnrollment3.setCourse(course3)

# Student 2 Enrollment
s2classEnrollment1 = ClassEnrollment(65492, "A+")
s2classEnrollment1.setCourse(course1)

s3classEnrollment1 = ClassEnrollment(89875, "F")
s3classEnrollment1.setCourse(course1)

# Student 3 Enrollment
s3classEnrollment2 = ClassEnrollment(24698, "D-")
s3classEnrollment2.setCourse(course3)

#No Student enrollment
noStudentEnrollment = ClassEnrollment(98671, "C")
noStudentEnrollment.setCourse(course1)

# Enroll student 1 in their class
student1.enroll2Class(s1classEnrollment1)
student1.enroll2Class(s1classEnrollment2)
student1.enroll2Class(s1classEnrollment3)

# Enroll student 2 in their class
student2.enroll2Class(s2classEnrollment1)

# Enroll student 3 in their class
student3.enroll2Class(s3classEnrollment1)
student3.enroll2Class(s3classEnrollment2)

# Print out the informatiuon about the students
printOutStudentInformation(student1.retrieiveByCWID())
printOutStudentInformation(student2.retrieiveByCWID())
printOutStudentInformation(student3.retrieiveByCWID())


#Save all of the class enrollments
s1classEnrollment1.save()
s1classEnrollment2.save()
s1classEnrollment3.save()
s2classEnrollment1.save()
s3classEnrollment1.save()
s3classEnrollment2.save()
s1classEnrollment3.save()
noStudentEnrollment.save()

#Test deletion of student 1's class enrollment and student
delete = True
if delete:
    s1classEnrollment1.delete()  # Delete class enrollment
    s1classEnrollment2.delete()  # Delete class enrollment
    s1classEnrollment3.delete()  # Delete class enrollment
    student1.delete()  # Delete student


