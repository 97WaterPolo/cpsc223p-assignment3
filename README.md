This is the basic structure of how my program works. I hope it satisfies all the requirements and such.  

In order to run this program put all the files (4 .py files) in a directory and execute the “School.py”

* Class Enrollment – Handles a class ID and the grade a student received in the class
* Course – Contains basic information about a course and all enrollments
* Student – Contains information about a student and their enrollments
* School – Creates and instantiates the objects and tests everything
In order to test the program, you can delete the “classEnrollments.csv” and the “students.csv”, it is auto generated and updated using pandas library. In the “Schoo.py” you can alter the test statements to use different variables and functions. To see whether the delete and save work, there are debug variables towards the end of the script you can change from True to False.  

The test program basically creates 3 students and enrolls them in different courses and at the end it prints out all the information about the students. It will then save the students and class enrollments to a file so that it could be loaded back at a later time. Also have the option to delete saved Students or ClassEnrollments from the file.  

If you use the original source code for this project, you will get the following results in the .csv files, this is with me testing the deletion by removing Student 1’s class enrollment and student objects!

![Image](http://siglerdev.us/photoupload/2020_05_02_19_28_53_Assignment3_C_Users_Xander_PycharmProjects_223P_Assignment3_..._classEnroll.png)