from Course import Course
import pandas as pd

class ClassEnrollment:
    def __init__(self, id, grade):
        self.__id = id
        self.__grade = grade
        self.__student = None
        self.__course = None


    @property
    def id(self):
        return self.__id

    @property
    def grade(self):
        return self.__grade

    @property
    def course(self):
        return self.__course

    def setStudent(self, stdt):
        self.__student = stdt

    def setCourse(self, course: Course):
        self.__course = course

    def delete(self):
        # first find the row index of this object
        df = pd.read_csv('classEnrollment.csv')
        row = df[df.id == self.__id]
        if len(row.index) != 0:
            df.drop(row.index[0], inplace=True)
            df.to_csv('classEnrollment.csv', index=False)

    def save(self):
        try:
            df = pd.read_csv('classEnrollment.csv')
            row = df[df.id == self.__id]
            if len(row.index) != 0:
                # update an existing object
                df.loc[row.index[0], 'id'] = self.__id
                df.loc[row.index[0], 'grade'] = self.__grade
                if self.__student != None:
                    df.loc[row.index[0], 'student'] = self.__student.cwid
                else:
                    df.loc[row.index[0], 'student'] = self.__student
            else:
                # new object
                cnt = len(df.index)
                if self.__student != None:
                    df.loc[cnt] = {'id': self.__id, 'grade': self.__grade, 'student': int(self.__student.cwid)}
                else:
                    df.loc[cnt] = {'id': self.__id, 'grade': self.__grade, 'student': self.__student}
        except:
            if self.__student != None:
                df = pd.DataFrame([[self.__id, self.grade, int(self.__student.cwid)]], columns=['id', 'grade', 'student'])
            else:
                df = pd.DataFrame([[self.__id, self.__grade, self.__student]], columns=['id', 'grade', 'student'])
                #
        df.to_csv('classEnrollment.csv', index=False)
        if self.__student != None:
            self.__student.save()
