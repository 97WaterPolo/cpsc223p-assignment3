from ClassEnrollment import ClassEnrollment
import pandas as pd


class Student:
    def __init__(self, cwid, firstname, lastname):
        self.__cwid = int(cwid)
        self.__firstName = firstname
        self.__lastName = lastname
        self.__enrollments = []  # Empty list of all the enrollments for a student

    @property
    def cwid(self):
        return self.__cwid

    @property
    def firstname(self):
        return self.__firstName

    @property
    def lastname(self):
        return self.__lastName

    def enroll2Class(self, classenrollment: ClassEnrollment):
        self.__enrollments.append(classenrollment)  # Enroll the student VIA Course Enrollment
        classenrollment.setStudent(self)

    def retrieiveByCWID(self):
        data = [self]
        for x in self.__enrollments:
            data.append(x)  # Return all the objects related to the Student object
        return data

    def delete(self):
        # first find the row index of this object
        df = pd.read_csv('students.csv')
        row = df[df.cwid == self.__cwid]
        if len(row.index) != 0:
            df.drop(row.index[0], inplace=True)
            df.to_csv('students.csv', index=False)

    def save(self):
        # client.csv
        try:
            df = pd.read_csv('students.csv')
            row = df[df.cwid == self.__cwid]
            if len(row.index) != 0:
                # update an existing object
                df.loc[row.index[0], 'cwid'] = self.__cwid
                df.loc[row.index[0], 'first'] = self.__firstName
                df.loc[row.index[0], 'last'] = self.__lastName
            else:
                # new object
                cnt = len(df.index)
                df.loc[cnt] = {'cwid': self.__cwid, 'first': self.__firstName, 'last': self.__lastName}
        except:
            df = pd.DataFrame([[self.__cwid, self.__firstName, self.__lastName]], columns=['cwid', 'first', 'last'])
                #
        df.to_csv('students.csv', index=False)
